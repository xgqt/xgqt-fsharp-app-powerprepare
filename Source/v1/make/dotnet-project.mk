# -*- make -*-

DOTNET-CONFIGURATION    := Release
DOTNET-PROJECT          :=

.PHONY: all
all:

.PHONY: clean
clean:
	$(FIND) . -type d -name ".cache" -exec $(RM) {} +

.PHONY: restore
restore:
	$(DOTNET) restore		\
		--verbosity quiet	\
		-maxCpuCount:1		\
		-nologo				\
		$(DOTNET-PROJECT)

.PHONY: build
build: restore
build:
	$(DOTNET) build			\
		--configuration $(DOTNET-CONFIGURATION)	\
		--no-restore		\
		--no-self-contained	\
		--verbosity quiet	\
		-maxCpuCount:1		\
		-nologo				\
		$(DOTNET-PROJECT)

.PHONY: test
test: build
test:
	$(DOTNET) test			\
		--configuration $(DOTNET-CONFIGURATION)	\
		--no-build          \
		--no-restore		\
		--verbosity minimal	\
		-maxCpuCount:1		\
		-nologo				\
		$(DOTNET-PROJECT)

.PHONY: pack
pack: build
pack:
	$(DOTNET) pack			\
		--configuration $(DOTNET-CONFIGURATION)	\
		--no-build          \
		--no-restore		\
		--verbosity minimal	\
		-maxCpuCount:1		\
		-nologo				\
		$(DOTNET-PROJECT)
