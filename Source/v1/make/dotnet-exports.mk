# -*- make -*-

# Less console noise.
export DOTNET_NOLOGO = 1
export DOTNET_SKIP_FIRST_TIME_EXPERIENCE = 1

# Sane build settings.
export DOTNET_ROLL_FORWARD = Major
export MSBUILDDISABLENODEREUSE = 1
export UseSharedCompilation = false

# Turn off telemetry.
export DOTNET_CLI_TELEMETRY_OPTOUT = 1
export POWERSHELL_TELEMETRY_OPTOUT = 1
export POWERSHELL_UPDATECHECK = 0
