#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
set -x
trap 'exit 128' INT

admin="$(realpath "$(dirname "${0}")")"
source="$(realpath "${admin}/../")"

cd "${source}"
make clean
make build
make test
git add --all --verbose "${source}"
