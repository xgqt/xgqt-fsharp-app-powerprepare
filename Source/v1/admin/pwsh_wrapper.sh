#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# You can force thew use of pwsh tool with "USE_PWSH_TOOL".

set -e
set -u
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

: "${USE_PWSH_TOOL:=}"
pwsh_exe=""

if [ "${USE_PWSH_TOOL}" != "YES" ] && which pwsh >/dev/null 2>&1 ; then
    pwsh_exe="pwsh"
else
    "${script_root}/restore_tools.sh"

    pwsh_exe="${script_root}/dotnet_wrapper.sh tool run pwsh"
fi

exec ${pwsh_exe} -NoLogo -NoProfile "${@}"
