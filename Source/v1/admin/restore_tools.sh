#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

source_root="$(realpath "${script_root}/../")"
cd "${source_root}"

stamp_directory="${source_root}/.cache/stamps"
restore_stamp_file="${stamp_directory}/dotnet_tools_restored.stamp"

if [ ! -f "${restore_stamp_file}" ] ; then
    set -x

    tool_manifest="${source_root}/.config/dotnet-tools.json"

    "${script_root}/dotnet_wrapper.sh" \
        tool restore --tool-manifest "${tool_manifest}" --verbosity minimal

    mkdir -p "${stamp_directory}"
    touch "${restore_stamp_file}"

    echo " Required .NET tools restored successfully"
fi
