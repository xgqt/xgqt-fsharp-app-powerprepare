#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

$sourceRoot = Join-Path -Resolve -Path $PSScriptRoot -ChildPath ".."
Set-Location -Path $sourceRoot

& make clean
& make build
& make test
& git add --all --verbose $sourceRoot
