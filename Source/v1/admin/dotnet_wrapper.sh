#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# This script will try to pick any available "dotnet" command.
# It will first search "dotnet" inside the "DOTNET_ROOT" directory.

set -e
set -u
trap 'exit 128' INT

: "${DOTNET_ROOT:=/usr/bin}"
dotnet_exe="${DOTNET_ROOT}/dotnet"

if [ ! -x "${dotnet_exe}" ] ; then
    printf " ERROR: The dotnet executable could not be found.\n" > /dev/stderr

    exit 1
fi

exec "${dotnet_exe}" "${@}"
