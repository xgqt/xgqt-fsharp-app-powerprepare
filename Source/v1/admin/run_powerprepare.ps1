#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

$sourceRoot = Join-Path -Resolve -Path $PSScriptRoot -ChildPath ".."
Set-Location -Path $sourceRoot

$dotnetExe = Join-Path -Resolve `
    -Path ($env:DOTNET_ROOT ? $env:DOTNET_ROOT : "/usr/bin") `
    -ChildPath "dotnet"
$powerprepareFsproj = Join-Path -Resolve `
    -Path $sourceRoot `
    -ChildPath "powerprepare-fsharp-app/src/PowerPrepare.App/PowerPrepare.App.fsproj"

& $dotnetExe run `
    --configuration Debug `
    --project "${powerprepareFsproj}" `
    -- $args
