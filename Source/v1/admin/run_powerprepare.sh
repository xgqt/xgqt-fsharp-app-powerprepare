#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

source_root="$(realpath "${script_root}/../")"
cd "${source_root}"

dotnet_exe="${DOTNET_ROOT:-/usr/bin}/dotnet"
powerprepare_fsproj="${source_root}/powerprepare-fsharp-app/src/PowerPrepare.App/PowerPrepare.App.fsproj"

exec "${dotnet_exe}" run                        \
     --configuration Debug                      \
     --project "${powerprepare_fsproj}"         \
     -- "${@}"
