{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  nativeBuildInputs = with pkgs.buildPackages; [
    dotnet-sdk_8
    gnumake

    # Fundamental packages
    git
    glibcLocales
  ];

  shellHook = ''
    make build
  '';
}
