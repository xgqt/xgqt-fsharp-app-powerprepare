// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

(*
   - read PowerPrepare.yaml
   - execute one of selected subcommands:
     - new
     - list
     - add
     - remove
     - install
   *)

open System
open System.Diagnostics
open System.IO
open System.Threading.Tasks

open Legivel.Attributes
open Legivel.Serialization

open SimpleLog.SimpleLog

open System.CommandLine
open System.CommandLine.Invocation

type PSMRecord =
    {
      // PowerShell module name
      [<YamlField("name")>]
      psmName: string

      // PowerShell module version - optional
      [<YamlField("version")>]
      psmVersion: string option }

type PSMRecords = PSMRecord list

let PowershellPathOption =
    new Option<string>([| "--powershell" |], "path to powershell to be used")

let PowershellAsToolOption =
    new Option<bool>([| "--powershell-tool" |], "run powershell as a dotnet tool")

let ConfigPathOption =
    new Option<string>([| "--config" |], "path to PowerPrepare configuration file")

ConfigPathOption.SetDefaultValue "./PowerPrepare.yaml"

let PickPwshRunner (options: Parsing.ParseResult) : string =
    let pwshPath = options.GetValueForOption PowershellPathOption

    if options.GetValueForOption PowershellAsToolOption then
        "dotnet tool run pwsh"
    elif not (String.IsNullOrEmpty pwshPath) then
        pwshPath
    else
        "pwsh"

let GetConfigPath (options: Parsing.ParseResult) : string =
    options.GetValueForOption ConfigPathOption

let ReadPowerPrepareYamlConfig (yamlConfigFilePath: string) : PSMRecords =
    if not (File.Exists yamlConfigFilePath) then
        LogMessage Error $"Configuration files was not found, given path: {yamlConfigFilePath}"

    let yamlContent = File.ReadAllText(yamlConfigFilePath)

    let resultList =
        DeserializeWithOptions<PSMRecords> [ MappingMode(MapYaml.WithCrossCheck) ] yamlContent

    let result = resultList[0]

    match result with
    | DeserializeResult.Success successInfo -> successInfo.Data
    | _ ->
        LogMessage Error "The configuration file parser returned error"

        "parsing failed" |> Exception |> raise

let PSMRecordToString (psmRecord: PSMRecord) : string =
    String.Concat(
        "- name: ",
        psmRecord.psmName,
        (match psmRecord.psmVersion with
         | Some v -> sprintf "\n  version: %s" v
         | None -> "")
    )

let SavePowerPrepareYamlConfig (psmRecords: PSMRecords) (yamlConfigFilePath: string) : unit =
    use writer = new StreamWriter(yamlConfigFilePath)

    do writer.Write("---\n")

    if psmRecords.Length > 0 then
        for psmRecord in psmRecords do
            do writer.Write(sprintf "%s\n" (PSMRecordToString psmRecord))
    else
        do writer.Write("[]\n")

    writer.Flush()

let CreateIfNotExistsPowerPrepareYamlConfig (yamlConfigFilePath: string) : unit =
    if not (File.Exists yamlConfigFilePath) then
        LogMessage Warning $"Configuration files was not found, given path: {yamlConfigFilePath}"
        LogMessage Warning $"Creating new configuration file at path: {yamlConfigFilePath}"

        SavePowerPrepareYamlConfig [] yamlConfigFilePath

let AddModuleToConfig (psmRecords: PSMRecords) (name: string) (version: string option) : PSMRecord list =
    psmRecords
    |> List.append [ ({ psmName = name; psmVersion = version }) ]
    |> List.sort

let RemoveModuleFromConfig (psmRecords: PSMRecords) (psmName: string) : PSMRecord list =
    psmRecords |> List.filter (fun record -> not (record.psmName = psmName))

let InstallPSMModule (pwshRunner: string) (psmRecord: PSMRecord) : unit =
    let installModuleOpts =
        sprintf
            "-AcceptLicense -Force -Scope CurrentUser %s -Name %s"
            (match psmRecord.psmVersion with
             | Some version -> $" -RequiredVersion {version}"
             | None -> "")
            psmRecord.psmName

    let pwshCommand =
        $"{pwshRunner} -NonInteractive -NoProfile -Command Install-Module {installModuleOpts}"

    let commandProcessStartInfo =
        if Environment.OSVersion.Platform = PlatformID.Win32NT then
            let argsString = sprintf "-Command %s" pwshCommand

            new ProcessStartInfo("pwsh.exe", argsString)
        else
            let argsString = sprintf "-c \"%s\"" pwshCommand

            new ProcessStartInfo("/bin/sh", argsString)

    commandProcessStartInfo.Environment.Add("POWERSHELL_TELEMETRY_OPTOUT", "1")
    commandProcessStartInfo.Environment.Add("POWERSHELL_UPDATECHECK", "0")

    commandProcessStartInfo.RedirectStandardOutput <- false
    commandProcessStartInfo.UseShellExecute <- false
    commandProcessStartInfo.CreateNoWindow <- true

    use commandProcess = new Process()

    commandProcess.StartInfo <- commandProcessStartInfo
    commandProcess.Start() |> ignore
    commandProcess.WaitForExit()

    if not (commandProcess.ExitCode = 0) then
        LogMessage Error "PowerShell exited with error"

        "errors occurred while running subcommand" |> Exception |> raise

let InstallModulesBasedOnConfig (pwshRunner: string) (psmRecords: PSMRecords) : unit =
    if psmRecords.Length > 0 then
        for psmRecord in psmRecords do
            LogMessage Debug $"Installing the {psmRecord.psmName} module"

            InstallPSMModule pwshRunner psmRecord

            LogMessage Success $"Successfully installed the {psmRecord.psmName} module"

        LogMessage Success $"Successfully installed all of the required Powershell modules"
    else
        LogMessage Warning "No Powershell modules to install"

let NewPSMCommandHandler (context: InvocationContext) : Task =
    task {
        let options = context.ParseResult

        let configPath = GetConfigPath options

        LogMessage Debug "Creating new PowerPrepare configuration file"

        SavePowerPrepareYamlConfig [] configPath

        LogMessage Success "Successfully created the configuration file"

        ()
    }

let NewPSMCommand: Command =
    let c = new Command("new", "create new PowerPrepare configuration file")
    c.SetHandler NewPSMCommandHandler
    c

let ListPSMCommandHandler (context: InvocationContext) : Task =
    task {
        let options = context.ParseResult

        let configPath = GetConfigPath options

        CreateIfNotExistsPowerPrepareYamlConfig configPath

        let records = ReadPowerPrepareYamlConfig configPath

        if records.Length > 0 then
            for record in records do
                printf " - %s" record.psmName

                match record.psmVersion with
                | Some version -> printfn " %s" version
                | None -> printfn ""
        else
            LogMessage Warning "Currently there are no PowerShell modules to list"

        ()
    }

let ListPSMCommand: Command =
    let c = new Command("list", "list required PowerShell modules")
    c.SetHandler ListPSMCommandHandler
    c

let AddPSMNameArgument =
    new Argument<string>("module-name", "module name to add to configuration")

let AddPSMVersionOption =
    new Option<string>([| "--mv"; "--module-version" |], "module version to add to configuration")

let AddPSMCommandHandler (context: InvocationContext) : Task =
    task {
        let options = context.ParseResult

        let configPath = GetConfigPath options
        let moduleName = options.GetValueForArgument AddPSMNameArgument
        let moduleVersion = options.GetValueForOption AddPSMVersionOption

        CreateIfNotExistsPowerPrepareYamlConfig configPath

        let oldRecords = ReadPowerPrepareYamlConfig configPath

        let newRecords =
            AddModuleToConfig
                oldRecords
                moduleName
                (match moduleVersion with
                 | none when String.IsNullOrEmpty(none) -> None
                 | some -> Some some)

        SavePowerPrepareYamlConfig newRecords configPath

        LogMessage Success $"Successfully added the {moduleName} module"

        ()
    }

let AddPSMCommand: Command =
    let c = new Command("add", "add a PowerShell module")
    c.SetHandler AddPSMCommandHandler
    c.AddArgument AddPSMNameArgument
    c.AddOption AddPSMVersionOption
    c

let RemovePSMNameArgument =
    new Argument<string>("module-name", "module name to remove from configuration")

let RemovePSMCommandHandler (context: InvocationContext) : Task =
    task {
        let options = context.ParseResult

        let configPath = GetConfigPath options
        let moduleName = options.GetValueForArgument RemovePSMNameArgument

        let oldRecords = ReadPowerPrepareYamlConfig configPath
        let newRecords = RemoveModuleFromConfig oldRecords moduleName

        SavePowerPrepareYamlConfig newRecords configPath

        LogMessage Success $"Successfully removed the {moduleName} module"

        ()
    }

let RemovePSMCommand: Command =
    let c = new Command("remove", "remove a PowerShell module")
    c.SetHandler RemovePSMCommandHandler
    c.AddArgument RemovePSMNameArgument
    c

let InstallPSMCommandHandler (context: InvocationContext) : Task =
    task {
        let options = context.ParseResult

        let configPath = GetConfigPath options
        let pwshRunner = PickPwshRunner options

        let records = ReadPowerPrepareYamlConfig configPath

        InstallModulesBasedOnConfig pwshRunner records

        ()
    }

let InstallPSMCommand: Command =
    let c = new Command("install", "install required PowerShell modules")
    c.SetHandler InstallPSMCommandHandler
    c

let CommandName = "powerprepare"
let CommandDescription = "define and manage Powershell module dependencies"

[<EntryPoint>]
let main (argv: string array) : int =
    let rootCommand = RootCommand(CommandName)

    rootCommand.Name <- CommandName
    rootCommand.Description <- CommandDescription

    rootCommand.AddOption ConfigPathOption
    rootCommand.AddOption PowershellPathOption
    rootCommand.AddOption PowershellAsToolOption

    rootCommand.AddCommand NewPSMCommand
    rootCommand.AddCommand ListPSMCommand
    rootCommand.AddCommand AddPSMCommand
    rootCommand.AddCommand RemovePSMCommand
    rootCommand.AddCommand InstallPSMCommand

    rootCommand.Invoke(argv)
