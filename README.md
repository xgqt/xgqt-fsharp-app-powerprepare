# PowerPrepare

PowerShell project dependency management tool.

## About

PowerPrepare is a simple CLI tool for PowerShell project dependency management.
It uses configuration files in the YAML format named `PowerPrepare.yaml`.
The configuration file includes a listing of packages that should be installed
on the system for a given project.
Generally all what somebody who wants to develop on a PowerShell project using
PowerPrepare has to do is first run `dotnet tool restore` to install
PowerPrepare and then run `dotnet tool run powerprepare install` to install
dependencies from the local project's `PowerPrepare.yaml`

## License

### Code

Code in this project is licensed under the MPL, version 2.0.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
